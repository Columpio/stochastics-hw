# Stochastics course homework
## Roadmap
- [x] Task 1
  - [x] Regression
  - [x] SVM
- [x] Task 2
- [x] Task 3
  - [x] Davies-Bouldin criteria and Calinski-Harabasz criteria
  - [x] Rand Index and Fowlkes-Mallows Index
