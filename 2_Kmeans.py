from PIL import Image
import numpy as np
import scipy.spatial
import os.path


def cluster_centroids(data, clusters, k=None):
    if k is None:
        k = np.max(clusters) + 1
    result = np.empty(shape=(k,) + data.shape[1:])
    for i in range(k):
        d = data[clusters == i]
        if d.size > 0:
            np.mean(d, axis=0, out=result[i])
    return result


def kmeans(data, k=None, centroids=None, steps=20):
    if centroids is not None and k is not None:
        assert(k == len(centroids))
    elif centroids is not None:
        k = len(centroids)
    elif k is not None:
        centroids = data[np.random.choice(np.arange(len(data)), k, False)]
    else:
        raise RuntimeError("Need a value for k or centroids.")

    for _ in range(max(steps, 1)):
        sqdists = scipy.spatial.distance.cdist(centroids, data, 'sqeuclidean')

        clusters = np.argmin(sqdists, axis=0)

        new_centroids = cluster_centroids(data, clusters, k)
        if np.array_equal(new_centroids, centroids):
            break

        centroids = new_centroids

    return centroids, clusters


def K_means(X, k, max_iters=100, repeat_runs=1):
    def F_3(centroids):
        return scipy.spatial.distance.cdist(centroids, centroids, 'sqeuclidean').max()
    assert repeat_runs >= 1
    ccs = [kmeans(X, k=k, steps=max_iters) for _ in range(repeat_runs)]
    centroids, clusters = min(ccs, key=lambda t: F_3(t[0]))
    return centroids, clusters


def save_image(new_X, output_image_file, shape):
    new_image = new_X.reshape(shape)
    Image.fromarray(new_image).save(output_image_file)


def convert_array(new_X):
    return new_X.astype(int).clip(0, 255).astype('uint8')


def main():
    for photo in ["grain", "lena", "peppers"]:
        input_image_file = os.path.join("data", photo + ".jpg")
        image = np.array(Image.open(input_image_file))
        X = image.reshape((image.shape[0] * image.shape[1], image.shape[2]))

        for k in [30, 50, 90]:
            output_image_file = os.path.join("data", "%s_%dk.jpg" % (photo, k))

            centroids, clusters = K_means(X, k=k, max_iters=10, repeat_runs=5)

            save_image(convert_array(centroids[clusters]), output_image_file, image.shape)


if __name__ == '__main__':
    main()
